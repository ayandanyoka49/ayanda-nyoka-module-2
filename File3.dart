// creating a class
class Name {
  var name;
  var category;
  var developer;
  var year;

  //Perceiving  apps

  showApp() {
    print("The app name to Upper case is : ${name.toUpperCase()}");
  }

  appInfo() {
    print("App of the year is : ${name}");
    print("sector/category is : ${category}");
    print("Developer is : ${developer}");
    print("Year is : ${year}");
  }
}

void main() {
  var app = new Name();
  app.name = "Educational Solution";
  app.category = "Education Sector";
  app.developer = "Ambani";
  app.year = "2021";

  app.appInfo();

  app.showApp();
}
